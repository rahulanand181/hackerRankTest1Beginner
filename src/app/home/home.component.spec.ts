import { ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HomeComponent } from './home.component';
import { Router } from '@angular/router';
import { routes } from '../app-routing.module';
import { ShopByCategoryComponent } from '../shop-by-category/shop-by-category.component';
import { CommonModule,Location } from '@angular/common';

class RouterStub{
  navigate(params:any){

  }
}

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let location: Location;
  let router: Router;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes),CommonModule,HttpClientTestingModule ],
        declarations: [HomeComponent, ShopByCategoryComponent],
     
      providers: [
        {provider: Router, useClass: RouterStub}
      ]
    }).compileComponents();

    router = TestBed.get(Router);
    location = TestBed.get(Location);

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check the brand name', () => {
    let brandName = fixture.debugElement.query(By.css('.navbar-brand'));
    let element:HTMLElement = brandName.nativeElement;

    expect(element.innerText).toBe('Beyond');
  });

  it('should check the click event on clicking shopByCategory', () => {
    let shopButton = fixture.debugElement.query(By.css('#home'));
    shopButton.triggerEventHandler('click', null);

    expect(component.displayCategories()).toBeDefined;
  });
  
  it('should redirect the user to home page',  fakeAsync(() => {
    router.navigate(["/shop"]).then(() => {
      expect(location.path()).toBe("/shop");
    });
}));


});
